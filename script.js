!(function () {
    // String, Numbers, Boolean, null, undefined
    const name = "Saulius";
    const age = 47;
  
    // Concatenation
    console.log("My name is " + name + " and I am " + age);
  
    const hello = `My name is ${name} and I am ${age}`;
  
    console.log(hello);
  })();
