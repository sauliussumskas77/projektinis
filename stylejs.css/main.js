!(function () {
    // String, Numbers, Boolean, null, undefined
    // const name = "Saulius";
    // const age = 47;
  
    // // Concatenation
    // console.log("My name is " + name + " and I am " + age);
  
    // const hello = `My name is ${name} and I am ${age}`;
  
    // console.log(hello);
    const spanId = document.querySelector("#myModalClose");
    const button = document.querySelector('#myBtn');
    const modal = document.querySelector('#myModal');

    button.addEventListener('click',function(e){
      console.log("button clicked");
      modal.classList.add('active')
    })

    spanId.addEventListener('click', function(e){
      console.log("close button clicked");
      modal.classList.remove('active')
    })
  
    console.log("button",button);


  })();
 